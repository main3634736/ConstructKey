package com.ranvargas.constructkey.specification.ppt.meetings;

import com.ranvargas.constructkey.domain.PullPlanTargetMeeting;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;

@Spec(path = "id", pathVars = "meetingId", spec = Equal.class)
public interface PullPlanTargetMeetingByPullPlanTargetMeetingIdSpec extends Specification<PullPlanTargetMeeting> {
}
