package com.ranvargas.constructkey.service;

import java.util.UUID;

import com.ranvargas.constructkey.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ranvargas.constructkey.client.CognitoClient;
import com.ranvargas.constructkey.domain.User;
import com.ranvargas.constructkey.domain.dto.UserRegistration;

@Service
public class PublicService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CognitoClient cognito;
	
	public User registerUser(UserRegistration registration) {
		
		String cognitoUUID = cognito.signUpUser(registration);
		
		User u = new User();
		u.setId(UUID.fromString(cognitoUUID));
		u.setEmail(registration.getEmail());
		u.setMobile(registration.getMobile());
		
		User createdUser = userRepository.save(u);
		
		
		return createdUser;
	}

}
