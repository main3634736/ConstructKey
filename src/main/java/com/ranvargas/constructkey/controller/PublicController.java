package com.ranvargas.constructkey.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ranvargas.constructkey.client.JWTClient;
import com.ranvargas.constructkey.domain.User;
import com.ranvargas.constructkey.domain.dto.UserRegistration;
import com.ranvargas.constructkey.service.PublicService;

@RestController
@RequestMapping("/public")
public class PublicController {

	Logger logger = LoggerFactory.getLogger(PublicController.class);
	
	@Autowired
	private JWTClient jwtClient;
	
	@Autowired
	private PublicService publicService;
	
	@PostMapping("/register")
	public ResponseEntity<?> createUser(@RequestBody UserRegistration registration) {
		return new ResponseEntity<User>(publicService.registerUser(registration), HttpStatus.CREATED);
	}
	
	@GetMapping("/health")
	public ResponseEntity<String> health() {
		return new ResponseEntity<String>("Ok", HttpStatus.OK);
	}
	
	@GetMapping("/key")
	public ResponseEntity<String> getPublicKey() {
		return new ResponseEntity<String>(jwtClient.getPublicKey(), HttpStatus.OK);
	}
}
