package com.ranvargas.constructkey.config;

import java.util.Optional;
import java.util.UUID;

import com.ranvargas.constructkey.domain.User;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwareImpl implements AuditorAware<User> {

	@Override
	public Optional<User> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication.getName().equals("anonymousUser")) {
			return Optional.empty();
		}
		return Optional.of(new User(UUID.fromString(authentication.getName())));
	}

}
