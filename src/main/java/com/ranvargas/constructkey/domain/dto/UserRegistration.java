package com.ranvargas.constructkey.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistration {

	private String email;
	private String mobile;
	
	private String password;
}
