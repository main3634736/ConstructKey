package com.ranvargas.constructkey.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserProjectInvitation {

	private String email;
	private String mobile;
}
